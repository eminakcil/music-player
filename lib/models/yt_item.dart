class YTItem {
  final String id;
  final String title;
  final String thumbnail;
  final String channel;
  String localPath;

  YTItem({
    this.id,
    this.title,
    this.thumbnail,
    this.channel,
    this.localPath,
  });

  factory YTItem.fromJson(Map<String, dynamic> json) {
    return YTItem(
      id: json['id'],
      title: json['title'],
      thumbnail: json['thumbnail'],
      channel: json['channel'],
      localPath: json['local_path'] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "id": this.id,
      "title": this.title,
      "thumbnail": this.thumbnail,
      "channel": this.channel,
      "local_path": this.localPath ?? '',
    };
  }
}
