import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:music_player/models/yt_item.dart';

class PlayerState with ChangeNotifier {
  AudioPlayer _audioPlayer;

  String title;
  AudioState audioState;

  String get durationText{
    if(_duration != null){
      return '${_duration.inMinutes.toInt()}:${(_duration.inSeconds % 60).toString().padLeft(2, "0")}';
    }
    return '00:00';
  }

  String get positionText{
    if (_position != null){
      return '${_position.inMinutes.toInt()}:${(_position.inSeconds % 60).toString().padLeft(2, "0")}';
    }
    return '00:00';
  }

  double get durationSecond{
    if (_duration != null){
      return _duration.inSeconds.toDouble();
    }
    return 1;
  }

  double get positionSecond{
    if (_position != null){
      return _position.inSeconds.toDouble();
    }
    return 0;
  }

  Duration _duration;
  Duration _position;

  PlayerState() {
    _init();
  }

  _init() {
    _audioPlayer = AudioPlayer();

    _audioPlayer.onPlayerStateChanged.listen((event) {
      switch (event) {
        case AudioPlayerState.STOPPED:
          audioState = AudioState.STOPPED;
          break;
        case AudioPlayerState.PLAYING:
          audioState = AudioState.PLAYING;
          break;
        case AudioPlayerState.PAUSED:
          audioState = AudioState.PAUSED;
          break;
        case AudioPlayerState.COMPLETED:
          audioState = AudioState.STOPPED;

          title = null;

          _position = null;
          _duration = null;

          break;
      }

      notifyListeners();
    });

    _audioPlayer.onDurationChanged.listen((Duration d) {
      // max duration
      _duration = d;
      notifyListeners();
    });

    _audioPlayer.onAudioPositionChanged.listen((Duration p) {
      // current position
      _position = p;
      notifyListeners();
    });
  }

  play(YTItem ytItem) async {
    String path = ytItem.localPath;
    title = ytItem.title;

    int response = await _audioPlayer.play(path, isLocal: true);

    if (response == 1) {
      notifyListeners();
    } else {
      print('Some error occured in playing from storage!');
    }
  }

  pause() async {
    int response = await _audioPlayer.pause();

    if (response == 1) {
      // success
    } else {
      print('Some error occured in pausing');
    }
  }

  resume() async {
    int response = await _audioPlayer.resume();

    if (response == 1) {
      // success
    } else {
      print('Some error occured in resuming');
    }
  }

  stopAudio() async {
    int response = await _audioPlayer.stop();

    if (response == 1) {
      // success
    } else {
      print('Some error occured in stopping');
    }
  }

  void seekToSecond(int second) {
    Duration newDuration = Duration(seconds: second);
    _audioPlayer.seek(newDuration);
  }
}

enum AudioState {
  STOPPED,
  PLAYING,
  PAUSED,
}
