import 'dart:async';

import 'package:music_player/models/yt_item.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper {
  static final DatabaseHelper _instance = DatabaseHelper.internal();

  factory DatabaseHelper() => _instance;

  final String tableYTItem = 'YTItemTable';
  final String columnId = 'id';
  final String columnThumbnail = 'thumbnail';
  final String columnTitle = 'title';
  final String columnChannel = 'channel';
  final String columnLocalPath = 'local_path';

  static Database _db;

  DatabaseHelper.internal();

  Future<Database> get db async {
    if (_db != null) {
      return _db;
    }
    _db = await initDb();
    return _db;
  }

  initDb() async {
    String databasesPath = await getDatabasesPath();
    String path = join(databasesPath, 'items.db');

//    await deleteDatabase(path);

    var db = await openDatabase(path, version: 1, onCreate: _onCreate);
    return db;
  }

  FutureOr<void> _onCreate(Database db, int version) async {
    await db.execute("CREATE TABLE $tableYTItem("
        "$columnId TEXT PRIMARY KEY,"
        "$columnTitle TEXT,"
        "$columnThumbnail Text,"
        "$columnChannel TEXT,"
        "$columnLocalPath TEXT"
        ")");
  }

  Future<int> saveYTItem(YTItem ytItem) async {
    var dbClient = await db;
    var result = await dbClient.insert(tableYTItem, ytItem.toJson());

    return result;
  }

  Future<List<YTItem>> getAllYTItems() async {
    var dbClient = await db;

    var results = await dbClient.rawQuery('SELECT * FROM $tableYTItem');

//    print(results);

    if (results.length > 0) {
      return List.generate(
          results.length, (index) => YTItem.fromJson(results[index]));
    }

    return null;
  }

  Future<YTItem> getYTItem(String id) async {
    var dbClient = await db;

    List<Map> results = await dbClient.query(tableYTItem,
        columns: [columnId, columnTitle, columnThumbnail, columnChannel, columnLocalPath],
        where: '$columnId = ?',
        whereArgs: [id]);

//    var results = await dbClient.rawQuery('SELECT * FROM $tableYTItem WHERE id = $id');

    if (results.length > 0) {
      return YTItem.fromJson(results.first);
    }

    return null;
  }

  Future<int> deleteYTItem(String id) async {
    var dbClient = await db;
    return await dbClient
        .delete(tableYTItem, where: '$columnId = ?', whereArgs: [id]);
//    return await dbClient.rawDelete('DELETE FROM $tableNote WHERE $columnId = $id');
  }

  Future<int> updateYTItem(YTItem ytItem) async {
    var dbClient = await db;
    return await dbClient.update(tableYTItem, ytItem.toJson(),
        where: "$columnId = ?", whereArgs: [ytItem.id]);
//    return await dbClient.rawUpdate(
//        'UPDATE $tableNote SET $columnTitle = \'${note.title}\', $columnDescription = \'${note.description}\' WHERE $columnId = ${note.id}');
  }

  Future close() async {
    var dbClient = await db;
    return dbClient.close();
  }
}
