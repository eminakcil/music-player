import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:music_player/models/yt_item.dart';

Future<List<YTItem>> search(String query) async {
  try {
    final response = await http.get('https://my-audio-api.herokuapp.com/search/?q=$query');
    var data = json.decode(response.body);
    if (data["status"] == "ok") {
      return List.generate(
          data["results"].length, (index) => YTItem.fromJson(data["results"][index]));
    } else {
      throw Exception("Failed to search");
    }
  } catch (e) {
      print(e);
      throw Exception("Failed to search");
  }
}