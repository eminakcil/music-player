import 'package:flutter/material.dart';
import 'package:music_player/screens/home_screen/library_screen.dart';
import 'package:music_player/screens/home_screen/play_screen.dart';
import 'package:music_player/screens/home_screen/search_screen.dart';

import 'home_screen/player_screen.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int selectedPage = 0;

  void changePage(int index){
    setState(() {
      selectedPage = index;
    });
  }

  final List<Widget> _children = [
    SearchScreen(),
    PlayerScreen(),
    LibraryScreen(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
//        backgroundColor: Color.fromARGB(255, 45, 49, 52),
        currentIndex: selectedPage,
        onTap: changePage,
        type: BottomNavigationBarType.fixed,
        iconSize: 35,
        fixedColor: Colors.black,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.search),
            title: SizedBox(
              height: 0,
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.play_arrow),
            title: SizedBox(
              height: 0,
            ),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.library_music),
            title: SizedBox(
              height: 0,
            ),
          ),
        ],
      ),
      body: _children[selectedPage],
    );
  }
}