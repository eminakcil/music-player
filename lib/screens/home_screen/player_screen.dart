import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:marquee/marquee.dart';
import 'package:music_player/state/player_state.dart';
import 'package:provider/provider.dart';

class PlayerScreen extends StatefulWidget {
  @override
  _PlayerScreenSetup createState() => _PlayerScreenSetup();
}

class _PlayerScreenSetup extends State<PlayerScreen> {
  @override
  Widget build(BuildContext context) {
    WidgetsFlutterBinding.ensureInitialized();
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
    ));
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(
          left: 10,
          right: 10,
          top: 40,
        ),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            colors: [
              Colors.brown,
              Colors.black87,
            ],
            end: Alignment.bottomCenter,
          ),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Container(
              margin: EdgeInsets.symmetric(horizontal: 22.0),
              width: double.infinity,
              color: Colors.black,
              height: 300.0,
            ),
            Expanded(child: Container()),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 22.0),
              width: double.infinity,
              height: 50,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Consumer<PlayerState>(
                      builder: (context, PlayerState playerState, child) {
                        String text = playerState.title;

                        if (text != null) {
                          return Marquee(
                            text: text,
                            style: TextStyle(
                              color: Colors.white,
                              fontFamily: "ProximaNova",
                              fontSize: 24,
                              fontWeight: FontWeight.bold,
                              wordSpacing: 0.2,
                            ),
                            crossAxisAlignment: CrossAxisAlignment.start,
                            blankSpace: 20.0,
                            velocity: 100.0,
                            startPadding: 0.0,
                            pauseAfterRound: Duration(seconds: 5),
                            accelerationDuration: Duration(seconds: 1),
                            accelerationCurve: Curves.linear,
                            decelerationDuration: Duration(milliseconds: 500),
                            decelerationCurve: Curves.easeOut,
                          );
                        }
                        return Text(
                          '',
                          style: TextStyle(
                            color: Colors.white,
                            fontFamily: "ProximaNova",
                            fontSize: 24,
                            fontWeight: FontWeight.bold,
                            wordSpacing: 0.2,
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
            Consumer<PlayerState>(
              builder: (context, PlayerState playerState, child) {
                String positionText = playerState.positionText;
                String durationText = playerState.durationText;

                double positionSecond = playerState.positionSecond;
                double durationSecond = playerState.durationSecond;

                Function onChanged = (double value) {
                  playerState.seekToSecond(value.toInt());
                };

                return Stack(
                  alignment: Alignment.bottomCenter,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(left: 5, right: 5),
                      width: double.infinity,
                      child: SliderTheme(
                        data: SliderTheme.of(context).copyWith(
                          activeTrackColor: Colors.white,
                          inactiveTrackColor: Colors.grey.shade600,
                          activeTickMarkColor: Colors.white,
                          thumbColor: Colors.white,
                          trackHeight: 3,
                          thumbShape: RoundSliderThumbShape(
                            enabledThumbRadius: 4,
                          ),
                        ),
                        child: Slider(
                          value: positionSecond,
                          min: 0.0,
                          max: durationSecond,
                          onChanged: onChanged,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 25, right: 25),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            positionText,
                            style: TextStyle(
                              color: Colors.grey.shade400,
                              fontFamily: "ProximaNovaThin",
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Text(
                            durationText,
                            style: TextStyle(
                              color: Colors.grey.shade400,
                              fontFamily: "ProximaNovaThin",
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                );
              },
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 22),
              width: double.infinity,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  IconButton(
                    icon: Icon(
                      Icons.call_split,
                      color: Colors.grey.shade400,
                    ),
                    onPressed: () {
//                      playSong();
                    },
                  ),
                  Icon(
                    Icons.skip_previous,
                    color: Colors.white,
                    size: 40,
                  ),
                  Container(
                    height: 90,
                    width: 90,
                    child: Center(
                      child: Consumer<PlayerState>(
                          builder: (context, PlayerState playerState, child) {
                        Icon _icon = Icon(
                          Icons.play_circle_filled,
                          color: Colors.white,
                        );
                        Function onPressed = () => null;

                        switch (playerState.audioState) {
                          case AudioState.PLAYING:
                            _icon = Icon(
                              Icons.pause_circle_filled,
                              color: Colors.white,
                            );
                            onPressed = () {
                              Provider.of<PlayerState>(context, listen: false)
                                  .pause();
                            };
                            break;
                          case AudioState.PAUSED:
                            _icon = Icon(
                              Icons.play_circle_filled,
                              color: Colors.white,
                            );
                            onPressed = () {
                              Provider.of<PlayerState>(context, listen: false)
                                  .resume();
                            };
                            break;
                          default:
                            {}
                            break;
                        }

                        return IconButton(
                          iconSize: 70,
                          alignment: Alignment.center,
                          icon: _icon,
                          onPressed: onPressed,
                        );
                      }),
                    ),
                  ),
                  Icon(
                    Icons.skip_next,
                    color: Colors.white,
                    size: 40,
                  ),
                  Icon(
                    Icons.loop,
                    color: Colors.grey.shade400,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

//Future<void> download() async {
//  print('start downloading');
//  String url = 'https://r6---sn-u0g3uxax3-xncs.googlevideo.com/videoplayback?expire=1599090867&ei=U9xPX-DYK4HbgQee47-ADw&ip=176.220.126.0&id=o-AGQkyEWecYBxWglrL0cm5XozS7yRlyxZmWXR7d3Hwpld&itag=140&source=youtube&requiressl=yes&mh=yp&mm=31%2C29&mn=sn-u0g3uxax3-xncs%2Csn-hgn7yn76&ms=au%2Crdu&mv=m&mvi=6&pl=21&initcwndbps=262500&vprv=1&mime=audio%2Fmp4&gir=yes&clen=5085151&dur=314.165&lmt=1594464992404371&mt=1599069168&fvip=5&keepalive=yes&c=WEB&txp=5431432&sparams=expire%2Cei%2Cip%2Cid%2Citag%2Csource%2Crequiressl%2Cvprv%2Cmime%2Cgir%2Cclen%2Cdur%2Clmt&sig=AOq0QJ8wRAIgTWkdAiV5v2SUnbU6jEbScze6zglfW3aaolFkO-eSbCECIGBVk1FkI8Y5WR4JPFvxucYeEJOjz1zSvM4eKJCSbTpO&lsparams=mh%2Cmm%2Cmn%2Cms%2Cmv%2Cmvi%2Cpl%2Cinitcwndbps&lsig=AG3C_xAwRgIhAIsZi6Igv_y21jgBdZGARyxjNe1cL7xSoHC9xj2uzWBQAiEA6Jba4h9U2qqw7DbcUh9LJQlTcryISh0R6ytMv1tJvmc%3D&ratebypass=yes';
//  String filename = 'deneme';
//  String path = await downloadFile(url, filename);
//  print('end downloading');
//  print('path: $path');
//}
//
//Future<void> playSong() async {
//  String path = '/data/user/0/com.emin.music_player/app_flutter/deneme';
//  AudioPlayer().play(path,isLocal: true);
//}


