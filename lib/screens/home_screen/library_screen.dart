import 'package:flutter/material.dart';
import 'package:marquee/marquee.dart';
import 'package:music_player/models/yt_item.dart';
import 'package:music_player/services/database_helper.dart';
import 'package:music_player/state/player_state.dart';
import 'package:provider/provider.dart';

DatabaseHelper db;

class LibraryScreen extends StatefulWidget {
  @override
  _LibraryScreenState createState() => _LibraryScreenState();
}

class _LibraryScreenState extends State<LibraryScreen> {
  void initState() {
    db = DatabaseHelper.internal();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            colors: [
              Colors.brown,
              Colors.black87,
            ],
            end: Alignment.bottomCenter,
          ),
        ),
        child: FutureBuilder<List<YTItem>>(
          future: db.getAllYTItems(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              List<YTItem> ytItemList = snapshot.data
                  .where((ytItem) => ytItem.localPath != '')
                  .toList();

              return ListView.builder(
                itemCount: ytItemList.length,
                itemBuilder: (context, index) =>
                    LibraryItem(ytItemList[index]),
              );
            } else if (snapshot.hasError) {
              return Text("${snapshot.error}");
            }
            // By default, show a loading spinner.
            return Center(child: CircularProgressIndicator());
          },
        ));
  }
}

class LibraryItem extends StatelessWidget {
  final YTItem ytItem;

  LibraryItem(this.ytItem);

  @override
  Widget build(BuildContext context) {
    Widget titleTextView = Text(
      ytItem.title,
      style: TextStyle(
        color: Colors.white,
        fontFamily: "ProximaNova",
        fontSize: 18.0,
        fontWeight: FontWeight.bold,
        wordSpacing: 0.2,
      ),
    );

    if (ytItem.title.length > 30) {
      titleTextView = Marquee(
        text: ytItem.title,
        style: TextStyle(
          color: Colors.white,
          fontFamily: "ProximaNova",
          fontSize: 18.0,
          fontWeight: FontWeight.bold,
          wordSpacing: 0.2,
        ),
        crossAxisAlignment: CrossAxisAlignment.start,
        blankSpace: 20.0,
        velocity: 100.0,
        startPadding: 0.0,
        pauseAfterRound: Duration(seconds: 5),
        accelerationDuration: Duration(seconds: 1),
        accelerationCurve: Curves.linear,
        decelerationDuration: Duration(milliseconds: 500),
        decelerationCurve: Curves.easeOut,
      );
    }

    return Container(
      margin: EdgeInsets.all(1),
      height: 50.0,
//      decoration: BoxDecoration(border: Border.all()),
      child: RawMaterialButton(
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: [
            Container(
              height: 50.0,
              width: 50.0,
              color: Colors.black,
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.all(4.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: titleTextView,
                    ),
                    Text(
                      ytItem.channel,
                      style: TextStyle(
                        color: Colors.grey.shade400,
                        fontFamily: "ProximaNovaThin",
                        fontWeight: FontWeight.bold,
                        fontSize: 12,
                        letterSpacing: 0.1,
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
        onPressed: () {
          Provider.of<PlayerState>(context, listen: false).play(ytItem);
        },
      ),
    );
  }
}
