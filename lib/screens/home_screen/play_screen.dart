import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:music_player/state/player_state.dart';
import 'package:provider/provider.dart';

class PlayScreen extends StatefulWidget {
  @override
  _PlayScreenState createState() => _PlayScreenState();
}

class _PlayScreenState extends State<PlayScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Consumer<PlayerState>(
              builder: (BuildContext context, PlayerState playerState,
                  Widget child) {
                return Text('${playerState.title}');
              },
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text('Position: '),
                Consumer<PlayerState>(
                  builder: (context, PlayerState playerState, child) {
                    return Text(playerState.positionText);
                  },
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text('Duration: '),
                Consumer<PlayerState>(
                  builder: (context, PlayerState playerState, child) {
                    return Text(playerState.durationText);
                  },
                ),
              ],
            ),
            Consumer<PlayerState>(
              builder: (context, PlayerState playerState, child) {
                return Slider(
                  min: 0.0,
                  max: playerState.durationSecond,
                  value: playerState.positionSecond,
                  onChanged: (double value) {
                    playerState.seekToSecond(value.toInt());
                  },
                );
              },
            ),
            Consumer<PlayerState>(
              builder: (BuildContext context, PlayerState playerState,
                  Widget child) {
                if (playerState.audioState != AudioState.PLAYING) {
                  return RaisedButton(
                    child: Text('play'),
                    onPressed: () {
                      Provider.of<PlayerState>(context, listen: false).resume();
                    },
                  );
                } else {
                  return RaisedButton(
                    child: Text('pause'),
                    onPressed: () {
                      Provider.of<PlayerState>(context, listen: false).pause();
                    },
                  );
                }
                return Text('${playerState.title}');
              },
            ),
          ],
        ),
      ),
    );
  }
}
