import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:music_player/models/yt_item.dart';
import 'package:music_player/services/data.dart';
import 'package:music_player/services/database_helper.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

DatabaseHelper db;

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  @override
  void initState() {
    db = DatabaseHelper.internal();
    super.initState();
  }

  final searchFieldController = TextEditingController();
  String searchString = '';

  Widget buildResut(String searchString) {
    return FutureBuilder<List<YTItem>>(
      future: search(searchString),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return ListView.builder(
            itemCount: snapshot.data.length,
            itemBuilder: (context, index) => ResultWidget(snapshot.data[index]),
          );
        } else if (snapshot.hasError) {
          return Text("${snapshot.error}");
        }
        // By default, show a loading spinner.
        return Center(child: CircularProgressIndicator());
      },
    );
  }

  bool checkStrIsReady(String string) {
    return string != '' && string != null && string.length > 3;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
//        color: Color.fromARGB(255, 35, 39, 42),
        child: Column(
          children: [
            TextField(
              controller: searchFieldController,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: 'search...',
              ),
              onEditingComplete: () {
                setState(() {
                  searchString = searchFieldController.text;
                });
              },
            ),
            Expanded(
              child: checkStrIsReady(searchString)
                  ? buildResut(searchString)
                  : Center(child: Text('search song')),
            )
          ],
        ),
      ),
    );
  }
}

class ResultWidget extends StatelessWidget {
  final YTItem searchObject;

  ResultWidget(this.searchObject);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(8),
      decoration: BoxDecoration(border: Border.all()),
      padding: EdgeInsets.all(8),
      width: double.infinity,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(searchObject.title, overflow: TextOverflow.clip),
                SizedBox(height: 8.0),
                Text(searchObject.channel),
              ],
            ),
          ),
          IconButton(
            icon: Icon(Icons.file_download),
            onPressed: () {
              downloadItem(searchObject);
            },
          )
        ],
      ),
    );
  }
}

Future<void> downloadItem(YTItem ytItem) async {
  YTItem existYTItem = await db.getYTItem(ytItem.id);

  if (existYTItem == null) {
    await db.saveYTItem(ytItem);
  } else {
    ytItem = existYTItem;
  }

  String filePath;

  if (ytItem.localPath == '') {
    filePath = await download(ytItem.id);
    if (filePath != null) {
      ytItem.localPath = filePath;
      await db.updateYTItem(ytItem);
    } else {
      print('indirilemedi');
    }
  } else {
    filePath = ytItem.localPath;
  }

  print(ytItem.localPath);
}

Future<String> download(String videoId) async {
  String url = await _getSourceUrl(videoId);
  String dir = (await getApplicationDocumentsDirectory()).path;
  String filename = videoId; // rastgele dosya adı
  String filePath = join(dir, filename);

  try {
    await Dio().download(url, filePath,
    onReceiveProgress: (count, total) {
      int percentage = ((count / total) * 100).floor();
      print('downloading: $percentage% in $total');
    },);
    return filePath;
  } catch (e) {
    throw Exception("Failed to download: $e");
  }
}

Future<String> _getSourceUrl(String videoId) async {
  String url = 'https://my-audio-api.herokuapp.com/?video_id=$videoId';
  try {
    final response = await http.get(url);
    var data = json.decode(response.body);
    if (data['status'] == 'ok') {
      String sourceUrl = data["audio_url"];
      return sourceUrl;
    } else {
      throw Exception("Failed to search: server error");
    }
  } catch (e) {
    throw Exception("Failed to search: $e");
  }
}
